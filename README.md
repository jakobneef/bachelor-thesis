# Bachelor thesis

My Bachelor thesis on the semi analytical computation of MHD bowshocks.

# Abstract
Abstract:
Aims. In their paper Schulreich and Breitschwerdt, 2011 considered astrophysical
bow shocks around galaxies or stars. They used a semianalytical method to calculate
their shape and the properties in the shock layer. They did, however, not consider
the influence of magnetic fields. The aim of this thesis is to show how magnetic
fields influence astrophysical bow shocks, and in particular see if the method used
is still applicable.
Results. We find that the method is not extendable to the MHD equations. We show
that even for the most convenient magnitudes and orientations of upstream flow, to
upstream magnetic field one is not able to reduce the problem in the same way.

# Usage

Just download the pdf file and enjoy reading.
Pictures and plots can be found in the figures subdirectory. 
